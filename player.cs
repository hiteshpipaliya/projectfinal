﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player : MonoBehaviour
{
    public float speed;
    public Text counttext;
   
    public Text WinText;
    private Rigidbody rb;
    private int count;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        SetCountText();
        WinText.text = "";
      
    }

    private void FixedUpdate()
    {
        float movementHorizantal = Input.GetAxis("Horizontal");
        float movementVertical = Input.GetAxis("Vertical");
        Vector3 Movement = new Vector3(movementHorizantal, 0.0f, movementVertical);
        rb.AddForce(Movement * speed);
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
      

        // Update is called once per frame

    }

    private void SetCountText(){
        counttext.text = "count ::" + count.ToString();
        if (count >= 12)
        {
            WinText.text = "You Win";
        }

    }
    
  
   
}
